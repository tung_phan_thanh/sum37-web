/* Last Update: 29/08/2016 17: 00 ; */
(function ($) {
    $.fn.getOwlOptions = function () {
        var elem = this,
            attr = {};

        if (elem && elem.length) $.each(elem.get(0).attributes, function (v, n) {
            n = n.nodeName || n.name;
            v = elem.attr(n); // relay on $.fn.attr, it makes some filtering and checks
            if (v != undefined && v !== false && n.indexOf('owl-') == 0) {
                if (v == 'false') {
                    v = false;
                }
                if (v == 'true') {
                    v = true;
                }
                key = n.replace('owl-', '');

                attr[key] = v;
            }
        })
        attr.autoHeight = true;
        attr.mouseDrag = false;
        attr.touchDrag = false;
        attr.smartSpeed = 700;
        return attr;
    }

    $(document).ready(function () {
        //$("html").niceScroll({styler:"fb",cursorcolor:"#000"});

        if (navigator.userAgent.match(/iPad/i)) {
            $('body').addClass('ipad-device')
        }
        $(window).load(function () {
            //$(this).scrollTop(0);
            setTimeout(function () {
                $('body').addClass('loaded');
                isScrolling = false;
            }, 2000);
        });
        $('.section-panel .section-background, .full-carousel-block .owl-carousel .item').each(function (index, el) {
            if (img = $(this).find('img')) {
                imgsrc = img.attr('src');
                $(this).css('background-image', 'url("' + imgsrc + '")');
            }
        });

        $('.owl-carousel').each(function (index, el) {
            if ($(this).parent().is(":visible")) {
                $owl = $(this);
                $opts = $owl.getOwlOptions();
                isReview = $owl.parent().parent().hasClass('reviews-block');
                if (isReview) {
                    delete $opts.items;
                    $opts.responsive = {
                        0: {
                            items: 1
                        },
                        1000: {
                            items: 3
                        }
                    }
                    $opts.autoHeight = false;
                }

                $owl.owlCarousel($opts);

                $owl.on('translate.owl.carousel', function (event) {
                    $target = event.currentTarget;
                    $('.prdbg', $target).each(function (index, el) {
                        $(this).removeClass('visible');
                        $(this).css('opacity', 0);
                    });

                    $('.animated', $target).each(function (index, el) {
                        $c = $(this).attr('rel');
                        $(this).addClass('hidden');
                        $(this).removeClass('visible');
                        $(this).removeClass($c);
                    });
                });

                $owl.on('translated.owl.carousel', function (event) {
                    $target = event.currentTarget;
                    $current = $(".owl-item.active", $target).children();
                    $('.animated.hidden', $current).removeClass('hidden');

                    if ($current.hasClass('reset-navbar')) {
                        $('body').removeClass('theme-light');
                        $('body').removeClass('theme-dark');
                        $('body').removeClass('menu-light');
                        $('body').removeClass('menu-dark');

                        $bodyClass = $current.attr('rel');
                        $('body').addClass($bodyClass);
                    }

                    if ($current.hasClass('animated-by-step')) {
                        var setAnimate = setInterval(function () {
                            hasElem = $('.animated', $current).not('.visible').length;
                            if (hasElem) {
                                Elem = $('.animated', $current).not('.visible').first();
                                $c = $(Elem).attr('rel');
                                $(Elem).addClass('visible');
                                $(Elem).addClass($c);
                            }
                            else {
                                clearInterval(setAnimate);
                            }
                        }, 500);
                    }
                    else {
                        $('.animated', $current).each(function (index, el) {
                            $effect = $(this).attr('rel');
                            $(this).addClass('visible');
                            $(this).addClass($effect);
                        });
                    }

                    setAnimate = setInterval(function () {
                        hasElem = $('.prdbg', $current).not('.visible').length;
                        if (hasElem) {
                            Elem = $('.prdbg', $current).not('.visible').first();
                            duration = parseInt($(Elem).attr("rel"));
                            $(Elem).animate({
                                opacity: 1,
                            }, duration, function () {
                                $(Elem).addClass('visible');
                            });
                        }
                        else {
                            clearInterval(setAnimate);
                        }
                    }, 500);
                });
            }
        });

        var $nav = $('#navigation'),
            $main = $('#main'),
            isMobile = ($(window).width() <= 1024);
        isScrolling = true;
        if ($('#introduction').length) {
            var $section = $('#introduction'),
                $video = $('#introduction video'),
                $videoMuted = false;

            //$video.get(0).play();
        }
        else {
            $section = $('.section-panel.active');
        }

        $('#footer .audio').click(function (event) {
            $videoMuted = !$videoMuted;
            $(this).toggleClass('mute');
            $video.prop('muted', $videoMuted); //mute
        });

        if (isMobile) {
            $(this).scrollTop(0);
            $(window).on('beforeunload', function () {
                $(window).scrollTop(0);
            });

            $('#main>div').each(function (index, el) {
                index = index + 1;
                sid = 'fullpage-section-' + index;
                $(this).attr('id', sid).addClass('section');
            });

            $('#main').fullpage({
                //events
                onLeave: function (index, nextIndex, direction) {
                    //remove action if need
                },
                afterLoad: function (anchorLink, index) {
                    $section_id = $("#fullpage-section-" + index + " .section-panel").attr("id");
                    $active_bid = $section.attr("id");
                    if ($section_id != $active_bid) {
                        $ost = $section;
                        $section = $('#' + $section_id);
                        $nav_item = $('#navigation ul.menu li a[href="#' + $section_id + '"]').parent();
                        activesection($section, $ost, $nav_item);
                    }
                },
            });

            // $(window).bind('scroll', resizeMenu);
            // function resizeMenu() {

            // 	if ( !$('body').hasClass('scrolling') ) {
            // 		h = isMobile ? 10 : 250;
            // 		if (!isScrolling && isMobile) {
            // 			$('.section-panel').not('.active').each(function () {
            // 				if ( $(this).offset().top - h < $(window).scrollTop() ) {
            // 					if ( !isScrolling ) {
            // 						$section_id = $(this).attr("id");
            // 						$active_bid = $section.attr("id");
            // 						if ($section_id != $active_bid){
            // 							$ost = $section;
            // 							$section = $('#' + $section_id);
            // 							$nav_item = $('#navigation ul.menu li a[href="#'+ $section_id +'"]').parent();
            // 							activesection($section, $ost, $nav_item);
            // 						}
            // 					}
            // 				}
            // 			});
            // 		}
            // 	}
            // }
        }
        else {
            var blockScroller = $("#main").blockScroll({
                fadeBlocks: false,
                scrollDuration: 1200,
                callback: function () {
                    $section_id = $("#main .block-active>section").attr("id");
                    $active_bid = $section.attr("id");
                    if ($section_id != $active_bid) {
                        $ost = $section;
                        $section = $('#' + $section_id);
                        $nav_item = $('#navigation ul.menu li a[href="#' + $section_id + '"]').parent();
                        activesection($section, $ost, $nav_item);
                    }
                }
            });
        }
        $('a', $nav).bind('click touch', scrollToMenuItem);
        function scrollToMenuItem(event) {
            event.preventDefault();
            $p = $(this).parent();
            $section_id = $(this).attr('href');

            if ($(this).hasClass('ex-link')) {
                location.href = $(this).attr('href');
                return true;
            }

            if ($('#header .btn-menu-mobile').is(':visible')) {
                $("#navigation").animate({
                    right: "-300px",
                }, 700);
            }

            if (!$p.hasClass('active')) {
                isScrolling = true;
                $('body').addClass('scrolling');

                if (isMobile) {
                    $("html, body").animate({scrollTop: $($section_id).offset().top}, 1000, function () {

                        $ost = $section;
                        $section = $($section_id);
                        activesection($section, $ost, $p);

                        setTimeout(function () {
                            $('body').removeClass('scrolling');
                            isScrolling = false;
                        }, 2000);
                    });
                }
                else {
                    $st = $($section_id);
                    section_index = $st.parent().index() + 1;
                    blockScroller.goto(section_index);
                }
            }

            return false;
        };

        function activesection($st, $ost, $nav_item) {
            //reset #main
            $('body').removeClass('theme-light');
            $('body').removeClass('theme-dark');
            $('body').removeClass('menu-light');
            $('body').removeClass('menu-dark');
            $('li.active', $nav).removeClass('active');

            if ($videoMuted == false) {
                $('#footer .audio').trigger('click');
            }

            $bodyClass = $st.attr('rel');
            $('body').addClass($bodyClass);
            $st.addClass('active');

            //setAnimate
            // if(isMobile){
            // 	var setAnimate = setInterval(function() {
            // 	var hasElem = $('.animated', $st).not('.visible').length;
            // 		if(hasElem){
            // 			Elem = $('.animated', $st).not('.visible').first();
            // 			$c = $(Elem).attr('rel');
            // 			$(Elem).addClass('visible');
            // 			$(Elem).addClass($c);
            // 		}
            // 		else{
            // 			clearInterval(setAnimate);
            // 		}
            // 	}, 400);
            // }
            // else{
            $('.animated', $st).each(function (index, el) {
                $effect = $(this).attr('rel');
                $(this).addClass('visible');
                $(this).addClass($effect);
            });
            // }
            //reset old section
            if (!isMobile) {
                $('.animated', $ost).each(function (index, el) {
                    $c = $(this).attr('rel');
                    $(this).removeClass('visible');
                    $(this).removeClass($c);
                });
            }

            $nav_item.addClass('active');
            setTimeout(function () {
                $('body').removeClass('scrolling');
                isScrolling = false;
            }, 2000);
        }

        $('#header .btn-menu-mobile').click(function (event) {
            $("#navigation").animate({
                right: "0",
            }, 700);
        });

        $('.congratulation-content .payment-method label').click(function (event) {
            $('.congratulation-content .payment-method label').removeClass('checked');
            $(this).addClass('checked');
        });

        $('#footer .join-experience span.btn-join').click(function (event) {
            $('#navigation ul.menu li a[href="#section-3"]').trigger('click');
            return false;
        });

        $('#navigation .close-button').click(function (event) {
            $("#navigation").animate({
                right: "-300px",
            }, 700);
        });

        $('input').bind('focusin focus', function (e) {
            e.preventDefault();
        })
        document.ontouchmove = function (event) {
            if ($('body').hasClass('scrolling')) {
                event.preventDefault();
            }
        }
    })

    var setUpBackgroundResponsive = (function () {
        var windowWidth = $(window).width();
        if (windowWidth <= 1024) {
            $('#section-3-background-img').attr('src', 'images/landing-page-mobile_bg.png');
        }
    })();

    var setUpGroupIconResponsive = function (callback) {
        var windowWidth = $(window).width();
        var itemGroup1 = $('.icon-group-1');
        var itemGroup2 = $('.icon-group-2');
        var itemGroup3 = $('.icon-group-3');
        var newGroupItemHtml = itemGroup1.html() + itemGroup2.html() + itemGroup3.html();
        console.log(newGroupItemHtml);
        if (windowWidth <= 1024) {
            $('#group-item-slider').html(newGroupItemHtml);
            callback();
        }
    }

    var setUpSliders = function () {
        var slider = $('#group-item-slider').owlCarousel({
            nav: true,
            dots: false,
            loop: true,
            autoplay: true,
            responsive: {
                200: {
                    items: 2
                },
                500: {
                    items: 3
                },
                800: {
                    items: 6
                }
            }
        });
    };

    setUpGroupIconResponsive(function () {
        setUpSliders()
    })

})(jQuery);
